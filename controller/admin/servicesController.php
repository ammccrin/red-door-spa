<?php

class servicesController extends adminController {
	
	function __construct(){
		parent::__construct("Service", "services");
	}
	
	function index(Array $params = []){
		$this->_viewData->hasCreateBtn = true;		


		parent::index($params);
	}

	function update(Array $arr = []){
		$ser = new $this->_model(isset($arr['id'])?$arr['id']:null);
		//$this->_viewData->services = \Model\Service::getList(['where sub_service = 0']);
		$this->_viewData->services = \Model\Service::getList(['where'=>' sub_service = 0', 'orderBy'=>' name DESC ']);
		parent::update($arr);
	}
  
}