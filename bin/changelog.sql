-- TRUNCATE public.product_images RESTART IDENTITY;
-- TRUNCATE public.jewelry RESTART IDENTITY;
-- TRUNCATE public.product RESTART IDENTITY;
-- TRUNCATE public.wishlist RESTART IDENTITY;
-- TRUNCATE public.wholesale_items RESTART IDENTITY;
-- TRUNCATE public.transaction RESTART IDENTITY;
-- TRUNCATE public.product_map RESTART IDENTITY;
-- TRUNCATE public.order_product_option RESTART IDENTITY;
-- TRUNCATE public.ring_material RESTART IDENTITY;
-- TRUNCATE public.address RESTART IDENTITY;
-- TRUNCATE public.page RESTART IDENTITY;
-- TRUNCATE public.coupon RESTART IDENTITY;
-- TRUNCATE public.user_favorite RESTART IDENTITY;
-- TRUNCATE public.wedding_band RESTART IDENTITY;
-- TRUNCATE public.order_products RESTART IDENTITY;
-- TRUNCATE public.category RESTART IDENTITY;
-- TRUNCATE public.ring RESTART IDENTITY;
-- TRUNCATE public.wholesale RESTART IDENTITY;
-- TRUNCATE public.banner RESTART IDENTITY;
-- TRUNCATE public.shipping_method RESTART IDENTITY;
-- TRUNCATE public.payment_profiles RESTART IDENTITY;
-- TRUNCATE public.contact RESTART IDENTITY;
-- TRUNCATE public.product_categories RESTART IDENTITY;
-- TRUNCATE public.credit RESTART IDENTITY;
-- TRUNCATE public.order RESTART IDENTITY;


-- October 10

ALTER TABLE public.contact ADD COLUMN donation NUMERIC(10,2);
ALTER TABLE public.contact ADD COLUMN form INTEGER;

-- October 11

CREATE SEQUENCE public.snapshot_contact_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.snapshot_contact
(
  id integer NOT NULL DEFAULT nextval('snapshot_contact_id_seq'::regclass),
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  image character varying COLLATE pg_catalog."default",
  contact_id integer NOT NULL,
  CONSTRAINT snapshot_contact_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

CREATE SEQUENCE public.gif_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.gif
(
  id integer NOT NULL DEFAULT nextval('gif_id_seq'::regclass),
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  image character varying COLLATE pg_catalog."default",
  frames character varying COLLATE pg_catalog."default",
  contact_id integer NOT NULL,
  CONSTRAINT gif_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

-- October 12

ALTER TABLE public.snapshot_contact ADD COLUMN in_slider SMALLINT DEFAULT 0;
ALTER TABLE public.gif ADD COLUMN in_slider SMALLINT DEFAULT 0;

-- May 7, 2018
CREATE SEQUENCE public.email_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.email
(
  id integer NOT NULL DEFAULT nextval('email_id_seq'::regclass),
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  email character varying,
  contact_id integer NOT NULL,
  CONSTRAINT email_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);


CREATE SEQUENCE public.highlight_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.highlight
(
  id integer NOT NULL DEFAULT nextval('highlight_id_seq'::regclass),
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  image character varying,
  display_order character varying,
  CONSTRAINT highlight_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

-- May 8, 2018

CREATE SEQUENCE public.hccsupport_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.hccsupport
(
  id integer NOT NULL DEFAULT nextval('hccsupport_id_seq'::regclass),
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  support_list character varying,
  phone character varying,
  CONSTRAINT hccsupport_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

CREATE SEQUENCE public.map_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.map
(
  id integer NOT NULL DEFAULT nextval('map_id_seq'::regclass),
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  map_image character varying,
  display_order character varying,
  CONSTRAINT map_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

-- May 9, 2018
CREATE SEQUENCE public.phone_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.phone
(
  id integer NOT NULL DEFAULT nextval('phone_id_seq'::regclass),
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  phone character varying,
  contact_id integer NOT NULL,
  CONSTRAINT phone_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);


CREATE SEQUENCE public.session_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.session
(
  id integer NOT NULL DEFAULT nextval('session_id_seq'::regclass),
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  title character varying,
  date_time character varying,
  CONSTRAINT session_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

--May 10, 2018
CREATE SEQUENCE public.support_user_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.support_user
(
  id integer NOT NULL DEFAULT nextval('support_user_id_seq'::regclass),
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  user_phone character varying,
  support_list_id integer,
  CONSTRAINT support_user_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);


CREATE SEQUENCE public.about_us_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

CREATE TABLE public.about_us
(
  id integer NOT NULL DEFAULT nextval('about_us_id_seq'::regclass),
  active smallint DEFAULT 1,
  insert_time timestamp without time zone DEFAULT now(),
  description character varying,
  CONSTRAINT about_us_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE session ALTER COLUMN date_time TYPE INTEGER USING date_time::INTEGER;

-- Oct 18, 2018
CREATE TABLE public.gift_card
(
    id SERIAL primary key,
    active SMALLINT NOT NULL DEFAULT 1,
    insert_time TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
    email CHARACTER VARYING COLLATE pg_catalog."default",
    amount CHARACTER VARYING COLLATE pg_catalog."default"
)

CREATE TABLE public.entry
(
    id SERIAL primary key,
    active SMALLINT NOT NULL DEFAULT 1,
    insert_time TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
    email CHARACTER VARYING COLLATE pg_catalog."default"
)
