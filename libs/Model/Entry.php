<?php

namespace Model;

class Entry extends \Emagid\Core\Model {
    static $tablename = "public.entry";

    public static $fields  =  [
        'email',
    ];

}