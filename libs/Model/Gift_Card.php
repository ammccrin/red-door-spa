<?php

namespace Model;

class Gift_Card extends \Emagid\Core\Model {
    static $tablename = "public.gift_card";

    public static $fields  =  [
        'email',
        'amount',
    ];

}