<section class='smartcity_page'>
    <div class='intro'>
        <p class='title'>Smart City 2018 Teams</p>
        <p>Click on a team to learn more</p>
    </div>

    <div class='content grey'>
        <div class='teams'>
           <div class='team' style="background-image: url('<?= FRONT_ASSETS ?>img/team1.jpg')">
                <div>
                    <p>100% Renewable</p>
                    <i class="fas fa-info"></i>
                </div>
           </div>
            <div class='more_info'>
              <p class='title'>Utility-as-a-service</p>
              <p>eEnergy procurement through green tariffs. RECs, IRPs, electrification of transportation and retail choice. Incentivize service bundles.</p>
              <i class="fas fa-times"></i>
            </div>

           <div class='team' style="background-image: url('<?= FRONT_ASSETS ?>img/team1.jpg')">
                <div>
                    <p>Smart Data</p>
                    <i class="fas fa-info"></i>
                </div>
           </div>
           <div class='team' style="background-image: url('<?= FRONT_ASSETS ?>img/team1.jpg')">
                <div>
                    <p>Transportation Electrification</p>
                    <i class="fas fa-info"></i>
                </div>
           </div>
           <div class='team' style="background-image: url('<?= FRONT_ASSETS ?>img/team1.jpg')">
                <div>
                    <p>Smarter Grid</p>
                    <i class="fas fa-info"></i>
                </div>
           </div>
           <div class='team' style="background-image: url('<?= FRONT_ASSETS ?>img/team1.jpg')">
                <div>
                    <p>Smart Energy Solutions</p>
                    <i class="fas fa-info"></i>
                </div>
           </div>
        </div>
    </div>
</section>

<script type="text/javascript">
  $('.teams').slick({
    arrows: true,
    slidesToShow: 1,
    autoplay: true,
    autoplaySpeed: 5000
  });

  $('.fa-info').click(function(){
    debugger
      $(this).parents('.team').next('.more_info').slideDown();
      $(this).parents('.team').next('.more_info').css('display', 'flex');
  });

    // $('.teams').masonry({
    //   itemSelector: '.team img'
    // });
    // $('.teams').imagesLoaded().progress( function() {
    //   $('.teams').masonry();
    // });  
</script>