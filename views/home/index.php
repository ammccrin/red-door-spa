<section class='background'>
    <div class='choices'>
        <button id='gift_card' class='button'>GIFT CARD</button>
        <button id='promotions' class='button'>PROMOTIONS</button>
        <button id='spa_day' class='button'>FREE SPA DAY</button>
    </div>
</section>

<div class='popup'>
    <div class='off_click'></div>
    <div class='holder gift_card'>
        <p>GIFT CARD</p>
        <p>Enter information below, and we'll send you a link to you're digital gift card!</p>
        <form id="gift_card" method="POST">
            <input type="email" name="gc_email" placeholder="Email" required>
            <input type="number" name="amount" placeholder="Amount" required>
            <input class='button red_button' id="btn_gift_card" type="submit" value='GET GIFT CARD'>
        </form>
    </div>

    <div class='holder spa_day'>
        <p>FREE SPA DAY</p>
        <p>Enter your email below for a free spa day!</p>
        <form id="sign_up" method="POST">
            <input placeholder="Email" type="email" name="signup_email" required>
            <input class='button red_button' id="btn_signup" type="submit" value='SIGN UP TO WIN'>
        </form>
    </div>
</div>

<div class='message'>
    <p>THANK YOU</p>
    <p></p>
</div>

<script type="text/javascript">
    $('#gift_card').click(function(){
        $('.popup').fadeIn();
        $('.popup').css('display', 'flex');
        $('.holder').hide();
        $('.gift_card').fadeIn();
    });

    $('#spa_day').click(function(){
        $('.popup').fadeIn();
        $('.popup').css('display', 'flex');
        $('.holder').hide();
        $('.spa_day').fadeIn();
    });

    $(".red_button").click(function(e){
        e.preventDefault();

        if ( $(this).parents('form')[0].checkValidity() ) {
            $('.message').css('width', '100%');
            if ( $(this).attr('id') == "btn_gift_card" ) {
                $('.message p:last-child').html("You'll recieve an email shortly!");
            }else {
                $('.message p:last-child').html("You've been entered, good luck!");
            }

            var timer;
            var self = this;
              timer = setTimeout(function () {
                  $(self).parents('form').submit();
                  $('.message p').hide();
              }, 4000);

            setTimeout(function(){
                $('.message p').fadeIn();
            }, 500);
        }


    });

    $('.off_click').click(function(){
        $('.holder').fadeOut();
        $('.popup').fadeOut();
    });
</script>

