<style type="text/css">
  div.jQKeyboardContainer {
    padding-top: 135px;
  }

  .sharex {
    position: absolute;
    top: 39px;
    left: 50%;
    transform: translateX(-50%);
    width: fit-content;
  }
</style>


<section class='feedback_page no_scroll'>
    <div class='intro'>
        <p class='title'>Smart City 2018 Teams</p>
        <p>Click on a team to learn more</p>
    </div>

    <div class='content grey'>
        <form class='feedback'>
          <input class='jQKeyboard' type="text" name="name" placeholder="Name">
          <textarea class='jQKeyboard' type="textarea" name="comments" placeholder="Comments"></textarea>
          <input class='button' type="submit" value="SUBMIT">
        </form>
    </div>
    <i class="fa fa-close sharex" style="font-size:36px"></i>
    <section id='share_alert'>
        <h3>Thank you!</h3>
        <p>We appreciate the feedback!</p>
      </section>
</section>


<script type="text/javascript">

    $('.jQKeyboard').click(function(){
      $('.sharex').fadeIn();
    });

    $('.sharex').click(function(){
      $('.jQKeyboardContainer').slideUp();
      $('.sharex').hide();
    });

    $('.feedback').submit(function(e){
      e.preventDefault();
      $('#share_alert').slideDown();
      $('#share_alert').css('display', 'flex');

      setTimeout(function(){
        $('.white').fadeIn();
        setTimeout(function(){
          window.location = '/';
        }, 1000)
      }, 3000);
    });

    // $('.teams').masonry({
    //   itemSelector: '.team img'
    // });
    // $('.teams').imagesLoaded().progress( function() {
    //   $('.teams').masonry();
    // });  
</script>