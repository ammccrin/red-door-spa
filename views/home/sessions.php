<main>
	<section class="home">

        <header class="inner_page">
            <a href='/' class="lefty"><img src="<?=FRONT_ASSETS?>img/logo.png"></a>
            <!-- <a href="#" class="qrscan">
                <img src="<?=FRONT_ASSETS?>img/cts-hcc-resource-center.png">
                <p style='color: black; font-size: 23px;'>Resource Center</p>
            </a> -->
            <a href="/" class="righty"><img src="<?=FRONT_ASSETS?>img/home_black.png"></a>
        </header> 
        <div class='background'></div>   

		<body>
			

            <!--  ==========  CHOICES  =============== -->
            <section class='choice_content'>
                <div class='intro_text'>
                    <div class="intro_inner">
                        <p class='large_intro float_left'>SESSIONS</p>
                        <a href="/" class="home_btn"><p>Back</p></a>
                    </div>
                    <div class="schedule_buttons">
                        <? foreach($model->sessions as $stamp => $date) {?>
                            <div class="day trig" data-target="#day<?=$stamp?>">
                                <p><?=$date['day']?></p>
                            </div>
                        <? } ?>
                        <div class="day trig" data-target="#day_four">
                            <p>Wednesday</p>
                        </div>
                    </div>

                </div>


                <? foreach($model->sessions as $stamp => $date) {?>
                    <div class="schedule_sessions" id="day<?=$stamp?>" style="display: none">
                        <? foreach ($date['sessions'] as $session){ ?>
                            <div class='timeit'>
                                <p class="small green dateTime"><?=$session->date_time?></p>
                                <p class="thin"><?=$session->title?></p>
                            </div>
                        <? } ?>
                    </div>
                <? } ?>
                
                
<!--                    WEDNESDAY-->    
<!--                STATIC - TIMIT FUNCTION NOT NEEDED-->
                <div class="schedule_sessions" id="day_four" style="display:none;">
                    <div>
                        <p class="small green">8:00 AM – 9:00 AM</p>
                        <p class="thin">Breakfast</p>
                        <p class="thin blue">Located in the Solution Center & Partner Exhibits (Mediterranean Ballroom)</p>
                    </div>
                    <div>
                        <p class="small green">9:00 AM – 10:15 AM</p>
                        <p class="thin">General Session: Industry Update</p>
                        <p class="thin blue">Coquina Ballroom</p>
                        <br>
                        <p class="thin">ERIC TOPOL, MD</p>
                        <p class="thin blue">Author, Founder and Director of Scripps Translational Science Institute (STSI) and the Dean of Digital Medicine. <br>Located in the Coquina Ballroom</p>
                    </div>
                    <div>
                        <p class="small green">10:15 AM – 10:45 AM</p>
                        <p class="thin">Break</p>
                        <p class="thin blue">Located in the Solution Center & Partner Exhibits (Mediterranean Ballroom) </p>
                    </div>
                    <div>
                        <p class="small green">10:45 AM – 12:15 PM</p>
                        <p class="thin">User Group Meetings</p>
                    </div>
                    <div>
                        <p class="small green">12:15 PM</p>
                        <p class="thin">Grab & Go Lunch</p>
                    </div>
                </div>
            </section>


<!--            TIMEIT SCRIPT-->
<script src="<?=FRONT_LIBS?>timeit/timeit.min.js"></script>


            <!--  ==========  FOOTER  =============== -->
            <footer>
                <a href="/home/photobooth">
                    <img src="<?=FRONT_ASSETS?>img/pic_cam.png">
                    <p>PHOTOBOOTH</p>
                </a>
                                <a href="/home/scan">
                    <img src="<?=FRONT_ASSETS?>img/qr-code.png">
                    <p>RESOURCES</p>
                </a>
                <a href="/home/about">
                    <img src="<?=FRONT_ASSETS?>img/trivia.png">
                    <p>ABOUT US</p>
                </a>
                <a href="/home/support">
                    <img src="<?=FRONT_ASSETS?>img/support.png">
                    <p>HCC SUPPORT</p>
                </a>
            </footer>





            <!--  ==========  QR  =============== -->
			<!-- <section id='photos' class='photos'>
                <h3 class='gif_text'>Scan your QR code below</h3>
				<video id="video" width="1900px" height="1690px" autoplay></video>
                <div id="embed" frameborder="0" allowfullscreen autoplay enablejsapi style="display: none">

                </div>
                <device type="media" onchange="update(this.data)"></device>
                <script>
                    function update(stream) {
                        document.querySelector('video').src = stream.url;
                    }
                </script>

                <div class='qr_message'>
                    <img src="<?=FRONT_ASSETS?>img/qr_code.jpg">
                    <p>Point your QR code at the camera to scan.</p>
                </div>

			</section> -->

			<canvas id="qr-canvas"style="display:none">
			</canvas>

			<!-- Choosing pictures -->
			<div id='results' style="display:none">

            </div>
            <div id='result' style="display:none">

            </div>

			<div id='qrimg' style="display:none">

			</div>

			<div id='webcamimg' style="display:none">

			</div>

			<!-- Alerts -->
			<section id='share_alert'>
                <img src="<?=FRONT_ASSETS?>img/check.png">
			</section>
<script>
    $(document).ready(function(){
        $('.dateTime').each(function(){
            var text = $(this).text();
            var start = moment.unix(text);
            $(this).text(start.format('hh:mm A'));
//            $(this).text(start.format('hh:mm A')+' - '+start.add(1,'h').format('hh:mm A'));
        });
        $('.day').click(function() {
            $('.day').removeClass("active");
            $(this).addClass("active");

        });
        $('.day.trig').click(function() {
            $('.schedule_sessions').hide();
            $($(this).data('target')).show();
        });
        $($('.day.trig')[0]).click();
    })
// $('.day#day_four_trig').click(function() {
//     $('.schedule_sessions').hide();
//     $('#day_four').show();
// });
</script>

</main>

<!-- images and gifs from the backend -->
<!--  -->
<script type="text/javascript" ></script>
<script type="text/javascript">
    var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);



    var player;
    function onYouTubeIframeAPIReady() {
        player = new YT.Player('embed', {
            playerVars: {autoplay:1},
            events: {
                'onStateChange': reload
            }
        });
    }
    function reload(event) {
        if(event.data === 0) {
            $('#embed').fadeOut();
            $('#video').removeClass('small_img');
            $('#share_alert').removeClass('small_check');
            $('#share_alert img').removeClass('small_check_img');
        }
    }
    function getId(url) {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        var match = url.match(regExp);

        if (match && match[2].length == 11) {
            return match[2];
        } else {
            return 'error';
        }
    }
    /*let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
    scanner.addListener('scan', function (a) {
        console.log(a);
        if(a.indexOf("http://") === 0 || a.indexOf("https://") === 0) {
            var yid = getId(a);

            console.log(yid);
            $('#share_alert').fadeIn();
            $('#share_alert').css('display', 'flex');

            setTimeout(function(){
                $('#share_alert').fadeOut();
                $('#embed').fadeIn();
                player.loadVideoById(yid);
            }, 2000);

            setTimeout(function(){
                $('#video').addClass('small_img');
                $('#share_alert').addClass('small_check');
                $('#share_alert img').addClass('small_check_img');
            }, 3500);


        }
    });
    Instascan.Camera.getCameras().then(function (cameras) {
        if (cameras.length > 0) {
            scanner.start(cameras[0]);
        } else {
            console.error('No cameras found.');
        }
    }).catch(function (e) {
        console.error(e);
    });*/



</script>


