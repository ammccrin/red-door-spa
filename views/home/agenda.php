<style type="text/css">
  div.jQKeyboardContainer {
    padding-top: 135px;
  }

  .sharex {
    position: absolute;
    top: 39px;
    left: 50%;
    transform: translateX(-50%);
    width: fit-content;
  }
</style>


<section class='agenda_page no_scroll'>
    <div class='intro'>
        <p class='title'>Agenda</p>
        <p>Monday 11/16/18</p>
    </div>

    <div class='content grey'>
        <div class='agendas'>
          <div class='agenda'>
            <p>Introduction & Safety Evacuation Plan</p>
            <p>8:15am – 8:17am <span>(2min)</span></p>
            <i class="fas fa-info"></i>
          </div>
          <div class='agenda'>
            <p>Agenda</p>
            <p>8:17am – 8:20am <span>(3 min)</span></p>
            <i class="fas fa-info"></i>
          </div>
          <div class='agenda'>
            <p>Introductory Video: AVANGRID Focused on the Future</p>
            <p>8:20am – 8:25am <span>(5 min)</span></p>
            <i class="fas fa-info"></i>
          </div>
          <div class='agenda'>
            <p>Opening remarks with Jim Torgerson (AVANGRID CEO)</p>
            <p>8:25am – 8:35am <span>(10 min)</span></p>
            <i class="fas fa-info"></i>
          </div>
          <div class='agenda'>
            <p>Forward 2020 Employee Ideas Spotlight + Awards</p>
            <p>8:35am – 8:50am <span>(15 min)</span></p>
            <i class="fas fa-info"></i>
          </div>
          <div class='agenda'>
            <p>Key Speaker – MA State Governor’s Representative</p>
            <p>8:50am – 9:20am <span>(30 min)</span></p>
            <i class="fas fa-info"></i>
          </div>

          <div class='agenda'>
            <p>BREAK</p>
            <p>9:20am – 9:35am <span>(15 min)</span></p>
          </div>
          <div class='agenda'>
            <p>Startups: MIT, Yale and Cornell Startups (10 min pitch + 5 min Q&A)</p>
            <p>9:35am – 10:20am <span>(45 min)</span></p>
            <i class="fas fa-info"></i>
          </div>
          <div class='agenda'>
            <p>Key Collaboration Partnership: Neuromesh, ITRON, Cisco (Blockchain Proof of Concept)</p>
            <p>10:20am – 10:50am <span>(30 min)</span> </p>
            <i class="fas fa-info"></i>
          </div>
          <div class='agenda'>
            <p>Key Speaker – Offshore (MIT) - Paul D Sclavounos</p>
            <p>10:50am – 11:20pm <span>(30 min)</span></p>
            <i class="fas fa-info"></i>
          </div>
            <div class='more_info'>
              <i class="fas fa-times"></i>
              <p class='time_a'>10:50am – 11:20pm <span>(30 min)</span></p>
              <img src="<?=FRONT_ASSETS?>img/paul.png">
              <p class='title'>Paul D. Scalavounos (MIT – Offshore)</p>
              <p>Professor of Mechanical Engineering and Naval Architecture</p>
            </div>

          <div class='agenda'>
            <p>Corporate Areas present Innovation Solutions (3x15 min each – Security and IT)</p>
            <p>11:20am – 11:55pm <span>(45 min)</span></p>
            <i class="fas fa-info"></i>
          </div>
          <div class='agenda'>
            <p>Key Speaker - Design Thinking (Yale) Rodrigo Canales</p>
            <p>11:55pm – 12:25pm <span>(30 min)</span></p>
            <i class="fas fa-info"></i>
          </div>

          <div class='agenda'>
            <p>LUNCH</p>
            <p>12:25pm – 12:55pm <span>(30 min)</span></p>
          </div>
          <div class='agenda'>
            <p>Key Collaboration Partnership: AR/VR MicroGrid (Tsai Center for Innovative Thinking at Yale)</p>
            <p>12:55pm – 1:25pm <span>(30 min)</span></p>
            <i class="fas fa-info"></i>
          </div>
          <div class='agenda'>
            <p>Breakout Sessions</p>
            <p>1:25pm – 1:55pm <span>(30 min)</span></p>
            <i class="fas fa-info"></i>
          </div>
          <div class='agenda'>
            <p>Smart Cities Competition (5 teams video + pitch business models) 12min ea.</p>
            <p>1:55pm – 2:55pm <span>(60 min)</span></p>
            <i class="fas fa-info"></i>
          </div>
          <div class='agenda'>
            <p>Smart City Competition Award</p>
            <p>2:55pm – 3:05pm <span>(10 min)</span></p>
            <i class="fas fa-info"></i>
          </div>
          <div class='agenda'>
            <p>Wrap up</p>
            <p>3:00pm – 3:15pm <span>(10min)</span> </p>
            <i class="fas fa-info"></i>
          </div>
        </div>
        </div>

    </div>


    <i class="fas fa-angle-up"></i>
    <i class="fas fa-angle-down"></i>
</section>


<script type="text/javascript">

    $('.jQKeyboard').click(function(){
      $('.sharex').fadeIn();
    });

    $('.sharex').click(function(){
      $('.jQKeyboardContainer').slideUp();
      $('.sharex').hide();
    });

    $('.feedback').submit(function(e){
      e.preventDefault();
      $('#share_alert').slideDown();
      $('#share_alert').css('display', 'flex');

      setTimeout(function(){
        $('.white').fadeIn();
        setTimeout(function(){
          window.location = '/';
        }, 1000)
      }, 3000);
    });

    $('.fa-angle-up').click(function(){
      $('.content').animate({
          scrollTop: $('.agendas').offset().top - 700
      }, 300);
    });

    $('.fa-angle-down').click(function(){
      $('.content').animate({
          scrollTop: $('.agendas').outerHeight() + $('.agendas').offset().top
      }, 300);
    });

    $('.agenda').click(function(){
      if ( $(this).children('.fa-info').length > 0 ){
        $(this).next('.more_info').slideDown();
        $(this).next('.more_info').css('display', 'flex');
      }
    });

    $('.fa-times').click(function(){
      $('.more_info').slideUp();
    });

    // $('.teams').masonry({
    //   itemSelector: '.team img'
    // });
    // $('.teams').imagesLoaded().progress( function() {
    //   $('.teams').masonry();
    // });  
</script>