<main>
	<section class="home" style='overflow: auto;'>

        <header class="inner_page">
            <a href='/' class="lefty"><img src="<?=FRONT_ASSETS?>img/logo.png"></a>
            <!-- <a href="#" class="qrscan">
                <img src="<?=FRONT_ASSETS?>img/cts-hcc-resource-center.png">
                <p style='color: black; font-size: 23px;'>Resource Center</p>
            </a> -->
            <a href="/" class="righty"><img src="<?=FRONT_ASSETS?>img/home_black.png"></a>
        </header> 
        <div class='background'></div>   

		<body>
			

            <!--  ==========  CHOICES  =============== -->
            <section class='choice_content'>
                <div class='intro_text'>
                    <div class="intro_inner">
                        <p class='large_intro float_left'>AGENDA</p>
                        <a href="/" class="home_btn"><p>Back</p></a>
                    </div>
                    <div class="schedule_buttons">
                        <div class="day active" id="day_one_trig">
                            <p>Sunday</p>
                        </div>
                        <div class="day" id="day_two_trig">
                            <p>Monday</p>
                        </div>
                        <div class="day" id="day_three_trig">
                            <p>Tuesday</p>
                        </div>
                        <div class="day" id="day_four_trig">
                            <p>Wednesday</p>
                        </div>
                    </div>
                </div>
                
                <div class="schedule_sessions" id="day_one">
                    <div class='timeit' data-end="2018-05-10 12:00">
                        <p class="small green">8:00 AM</p>
                        <p class="thin">Golf Tournament</p>
                    </div>
                    <div class='timeit' data-end="2018-05-20 15:00">
                        <p class="small green">12:00 PM - 3:00 PM</p>
                        <p class="thin">Registration, Solution Center & Partner Exhibits Open</p>
                    </div>
                    <div class='timeit' data-end="2018-05-20 19:30">
                        <p class="small green">5:00 PM - 7:30 PM</p>
                        <p class="thin">Welcome Address and Keynote<br> LINDSEY VONN</p>
                        <p class="thin blue">Located in the Coquina Ballroom</p>
                    </div>
                    <div class='timeit' data-end="2018-05-20 22:00">
                        <p class="small green">7:30 PM - 10:00 PM</p>
                        <p class="thin">Opening Partner Reception</p>
                        <p class="thin blue">Located in the Solution Center & Partner Exhibits (Mediterranean Ballroom)</p>
                    </div>

                    <div class='timeit' data-start="2018-05-20 22:00">
                        <p class="small green">No More Events</p>
                        <p class="thin">All events for this day have occured</p>
                    </div>
                </div>
                
                <div class="schedule_sessions" id="day_two" style="display:none;">
                    <div data-end="2018-05-21 8:00">
                        <p class="small green">7:00 AM – 8:00 AM</p>
                        <p class="thin">Breakfast</p>
                        <p class="thin blue">Located in the Solution Center and Partner Exhibits.</p>
                    </div>
                    <div data-end="2018-05-21 9:30">
                        <p class="small green">8:00 AM – 9:30 AM</p>
                        <p class="thin">General Session</p>
                        <p class="thin blue">Coquina Ballroom</p>
                    </div>
                    <div data-end="2018-05-21 9:45">
                        <p class="small green">9:30 AM – 9:45 AM</p>
                        <p class="thin">Break</p>
                        <p class="thin blue">Located in the Solution Center & Partner Exhibits (Mediterranean Ballroom) </p>
                    </div>
                    <div data-end="2018-05-21 11:15">
                        <p class="small green">9:45 AM – 11:15 AM</p>
                        <p class="thin">Solution General Sessions</p>
                    </div>
                    <div data-end="2018-05-21 12:30">
                        <p class="small green">11:30 AM – 12:30 PM</p>
                        <p class="thin">Solutions, Services & Industry Breakouts</p>
                    </div>
                    <div data-end="2018-05-21 13:30">
                        <p class="small green">12:30 PM – 1:30 PM</p>
                        <p class="thin">Lunch</p>
                        <p class="thin blue">Located in the Solution Center & Partner Exhibits (Mediterranean Ballroom) </p>
                    </div>
                    <div data-end="2018-05-21 14:30">
                        <p class="small green">1:30 PM – 2:30 PM</p>
                        <p class="thin">Solutions, Services & Industry Breakouts</p>
                    </div>
                    <div data-end="2018-05-21 15:45">
                        <p class="small green">2:45 PM – 3:45 PM</p>
                        <p class="thin">Solutions, Services & Industry Breakouts</p>
                    </div>
                    <div data-end="2018-05-21 16:15">
                        <p class="small green">3:45 PM – 4:15 PM</p>
                        <p class="thin">Break</p>
                        <p class="thin blue">Located in the Solution Center & Partner Exhibits (Mediterranean Ballroom) </p>
                    </div>
                    <div data-end="2018-05-21 17:15">
                        <p class="small green">4:15 PM – 5:15 PM</p>
                        <p class="thin">Solutions, Services & Industry Breakouts</p>
                    </div>
                    <div data-end="2018-05-21 22:00">
                        <p class="small green">5:15 PM</p>
                        <p class="thin">Open Evening</p>
                        <p class="thin blue">Dinner on your own.</p>
                    </div>

                    <div class='timeit' data-start="2018-05-21 22:00">
                        <p class="small green">No More Events</p>
                        <p class="thin">All events for this day have occured</p>
                    </div>
                </div>
                
<!--                TUESDAY-->
                <div class="schedule_sessions" id="day_three" style="display:none;">
                    
                    <div data-end="2018-05-22 7:30">
                        <p class="small green">6:45 AM – 7:30 AM</p>
                        <p class="thin">Breakfast</p>
                        <p class="thin blue">Located in the Solution Center & Partner Exhibits (Mediterranean Ballroom)</p>
                    </div>
                    <div data-end="2018-05-22 8:30">
                        <p class="small green">7:30 AM – 8:30 AM</p>
                        <p class="thin">General Session: Women Empowered</p>
                        <p class="thin blue">ALL ATTENDEES WELCOME - Coquina Ballroom</p>
                        <br>
                        <p class="thin">JUDY SPITZ</p>
                        <p class="thin blue">A female CIO leading a global, Fortune 10 technology organization and challenges organizations to develop a culture of inclusive leadership.</p>
                    </div>
                    <div data-end="2018-05-22 9:45">
                        <p class="small green">8:45 AM – 9:45 AM</p>
                        <p class="thin">Solutions, Services & Industry Breakouts</p>
                    </div>
                    <div data-end="2018-05-22 10:15">
                        <p class="small green">9:45 AM – 10:15 AM</p>
                        <p class="thin">Break</p>
                        <p class="thin blue">Located in the Solution Center & Partner Exhibits (Mediterranean Ballroom)</p>
                    </div>
                    
                    <div data-end="2018-05-22 11:15">
                        <p class="small green">10:15 AM – 11:15 AM</p>
                        <p class="thin">Client, Partner & Industry Breakouts</p>
                    </div>
                    <div data-end="2018-05-22 12:30">
                        <p class="small green">11:30 AM – 12:30 PM</p>
                        <p class="thin">Client, Partner & Industry Breakouts</p>
                    </div>
                    <div data-end="2018-05-22 13:30">
                        <p class="small green">12:30 PM – 1:30 PM</p>
                        <p class="thin">Lunch</p>
                        <p class="thin blue">Located in the Solution Center & Partner Exhibits (Mediterranean Ballroom)</p>
                    </div>
                    
                    <div data-end="2018-05-22 14:30">
                        <p class="small green">1:30 PM – 2:30 PM</p>
                        <p class="thin">Client, Partner & Industry Breakouts</p>
                    </div>
                    <div data-end="2018-05-22 18:00">
                        <p class="small green">2:30 PM – 6:00 PM</p>
                        <p class="thin">Free Time</p>
                    </div>
                    <div data-end="2018-05-22 22:00">
                        <p class="small green">6:00 PM</p>
                        <p class="thin">Theme Event</p>
                        <p class="thin blue">Buses depart from hotel at 6:00 pm</p>
                    </div>

                    <div class='timeit' data-start="2018-05-22 22:00">
                        <p class="small green">No More Events</p>
                        <p class="thin">All events for this day have occured</p>
                    </div>
                </div>
                
<!--                WEDNESDAY-->
                <div class="schedule_sessions" id="day_four" style="display:none;">
                    <div data-end="2018-05-23 9:00">
                        <p class="small green">8:00 AM – 9:00 AM</p>
                        <p class="thin">Breakfast</p>
                        <p class="thin blue">Located in the Solution Center & Partner Exhibits (Mediterranean Ballroom)</p>
                    </div>
                    <div data-end="2018-05-23 10:15">
                        <p class="small green">9:00 AM – 10:15 AM</p>
                        <p class="thin">General Session: Industry Update</p>
                        <p class="thin blue">Coquina Ballroom</p>
                        <br>
                        <p class="thin">ERIC TOPOL, MD</p>
                        <p class="thin blue">Author, Founder and Director of Scripps Translational Science Institute (STSI) and the Dean of Digital Medicine. <br>Located in the Coquina Ballroom</p>
                    </div>
                    <div data-end="2018-05-23 10:45">
                        <p class="small green">10:15 AM – 10:45 AM</p>
                        <p class="thin">Break</p>
                        <p class="thin blue">Located in the Solution Center & Partner Exhibits (Mediterranean Ballroom) </p>
                    </div>
                    <div data-end="2018-05-23 12:15">
                        <p class="small green">10:45 AM – 12:15 PM</p>
                        <p class="thin">User Group Meetings</p>
                    </div>
                    <div data-end="2018-05-23 14:15">
                        <p class="small green">12:15 PM</p>
                        <p class="thin">Grab & Go Lunch</p>
                    </div>
                    <div class='timeit' data-start="2018-05-22 14:15">
                        <p class="small green">No More Events</p>
                        <p class="thin">All events for this day have occured</p>
                    </div>
                </div>
            </section>

<script>
$('.day').click(function() {
    $('.day').removeClass("active");
    $(this).addClass("active");
    
});
$('.day#day_one_trig').click(function() {
    $('.schedule_sessions').hide();
    $('#day_one').show(); 
});
$('.day#day_two_trig').click(function() {
    $('.schedule_sessions').hide();
    $('#day_two').show(); 
});
$('.day#day_three_trig').click(function() {
    $('.schedule_sessions').hide();
    $('#day_three').show(); 
});
$('.day#day_four_trig').click(function() {
    $('.schedule_sessions').hide();
    $('#day_four').show(); 
});
</script>


            <!--  ==========  FOOTER  =============== -->
            <footer>
                <a href="/home/photobooth">
                    <img src="<?=FRONT_ASSETS?>img/pic_cam.png">
                    <p>PHOTOBOOTH</p>
                </a>
                                <a href="/home/scan">
                    <img src="<?=FRONT_ASSETS?>img/qr-code.png">
                    <p>RESOURCES</p>
                </a>
                <a href="/home/about">
                    <img src="<?=FRONT_ASSETS?>img/trivia.png">
                    <p>ABOUT US</p>
                </a>
                <a href="/home/support">
                    <img src="<?=FRONT_ASSETS?>img/support.png">
                    <p>HCC SUPPORT</p>
                </a>
            </footer>





            <!--  ==========  QR  =============== -->
			<!-- <section id='photos' class='photos'>
                <h3 class='gif_text'>Scan your QR code below</h3>
				<video id="video" width="1900px" height="1690px" autoplay></video>
                <div id="embed" frameborder="0" allowfullscreen autoplay enablejsapi style="display: none">

                </div>
                <device type="media" onchange="update(this.data)"></device>
                <script>
                    function update(stream) {
                        document.querySelector('video').src = stream.url;
                    }
                </script>

                <div class='qr_message'>
                    <img src="<?=FRONT_ASSETS?>img/qr_code.jpg">
                    <p>Point your QR code at the camera to scan.</p>
                </div>

			</section> -->

			<canvas id="qr-canvas"style="display:none">
			</canvas>

			<!-- Choosing pictures -->
			<div id='results' style="display:none">

            </div>
            <div id='result' style="display:none">

            </div>

			<div id='qrimg' style="display:none">

			</div>

			<div id='webcamimg' style="display:none">

			</div>

			<!-- Alerts -->
			<section id='share_alert'>
                <img src="<?=FRONT_ASSETS?>img/check.png">
			</section>


</main>

<!-- images and gifs from the backend -->
<!--  -->
<script src="<?=FRONT_LIBS?>timeit/timeit.min.js"></script>

<script type="text/javascript">
    var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);



    var player;
    function onYouTubeIframeAPIReady() {
        player = new YT.Player('embed', {
            playerVars: {autoplay:1},
            events: {
                'onStateChange': reload
            }
        });
    }
    function reload(event) {
        if(event.data === 0) {
            $('#embed').fadeOut();
            $('#video').removeClass('small_img');
            $('#share_alert').removeClass('small_check');
            $('#share_alert img').removeClass('small_check_img');
        }
    }
    function getId(url) {
        var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        var match = url.match(regExp);

        if (match && match[2].length == 11) {
            return match[2];
        } else {
            return 'error';
        }
    }
    let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
    scanner.addListener('scan', function (a) {
        console.log(a);
        if(a.indexOf("http://") === 0 || a.indexOf("https://") === 0) {
            var yid = getId(a);

            console.log(yid);
            $('#share_alert').fadeIn();
            $('#share_alert').css('display', 'flex');

            setTimeout(function(){
                $('#share_alert').fadeOut();
                $('#embed').fadeIn();
                player.loadVideoById(yid);
            }, 2000);

            setTimeout(function(){
                $('#video').addClass('small_img');
                $('#share_alert').addClass('small_check');
                $('#share_alert img').addClass('small_check_img');
            }, 3500);

            
        }
    });
    Instascan.Camera.getCameras().then(function (cameras) {
        if (cameras.length > 0) {
            scanner.start(cameras[0]);
        } else {
            console.error('No cameras found.');
        }
    }).catch(function (e) {
        console.error(e);
    });



</script>


