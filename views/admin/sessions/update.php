<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active">
			<input type="hidden" name="id" value="<?php echo $model->session->id; ?>" />
			<input name="token" type="hidden" value="<?php echo get_token();?>" />
			<div class="row">
				<div class="col-md-12">
					<div class="box">
						<h4>HCC Session Details</h4>
						<div class="form-group">
                            <label>Session Title</label>
                            <?php echo $model->form->editorFor("title"); ?>
                        </div>
						<div class="form-group">
                            <label>Date & Time</label> 
                            <input id="hccdatetime" type="text" value="" />
                            <input type="hidden" name="date_time" value="<?php echo $model->session->date_time; ?>" />
                        </div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<div class="form-group">
		<button type="submit" class="btn btn-save">Save</button>
	</div>
</form>

<?php footer(); ?>
<link rel = "stylesheet" type = "text/css" href = "<?=ADMIN_CSS.'jquery.datetimepicker.css'?>">
<script src="<?=ADMIN_JS.'plugins/jquery.datetimepicker.full.min.js'?>"></script>

<script type="text/javascript">
	$('#hccdatetime').datetimepicker({
		format: "D., M d - h:i A",
		formatTime: "h:i A",
		step:15
	});
    $('#hccdatetime').val(moment.unix($('[name=date_time]').val()).format('ddd., MMM DD - hh:mm A'));
    $('#hccdatetime').change(function(){
       $('[name=date_time]').val(moment($('#hccdatetime').val(),'ddd., MMM DD - hh:mm A',true).utc().unix());
    }).change();
</script>